FROM node:14-alpine
WORKDIR /var/www
COPY package*.json ./
RUN addgroup -S nodeapp && \
    adduser -S -G nodeapp nodeapp && \
    npm install && npm install pm2 -g && \
    npm audit fix
USER nodeapp
ENV NODE_OPTIONS="--max-old-space-size=512"
COPY . .
EXPOSE 3000
CMD ["pm2-runtime", "index.js", "--name", "retoDevOpsService"]