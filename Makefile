.PHONY: help

help: ## Print this help.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'
build: ## Build a dockerimage
	docker build -t reto-devops .
run: ## Run a dockerimage in a port
	make build && docker run -dp $(port):3000 reto-devops
ps: ## Show running containers
	docker ps
ps_all: ## Show all containers
	docker ps -a
compose_up: ## Up docker-compose file
	docker-compose up -d
compose_down: ## Down docker-compose file
	docker-compose down
deploy_pipeline: ## Start new pipeline
	git checkout development && git commit --allow-empty -m "style: trigger pipeline with make file at $(shell date)" && git push origin development
merge_main: ## Update main branch with development
	git checkout master && merge development